Name: openstack-helm-images
Version: 1072f9de5ca0b30a343208142905c002d77f2a1a
Release: 0
Summary: Images for use with OpenStack Helm

License: ASL 2.0
URL: https://opendev.org/openstack/openstack-helm-images
Source0: openstack-helm-images-%{version}.tar.gz

BuildArch: noarch

Requires: docker

%description
Images for use with OpenStack Helm

%prep
%setup -q -n openstack-helm-images-%{version}

%build

%install
install -d -m755 %{buildroot}%{_datadir}/openstack-helm
cp -vr %{_builddir}/openstack-helm-images-%{version} %{buildroot}%{_datadir}/openstack-helm/openstack-helm-images

%files
%{_datadir}/openstack-helm/openstack-helm-images

%changelog
* Tue Aug 30 2022 Yinuo Deng <i@dyn.im> - 1072f9de5ca0b30a343208142905c002d77f2a1a-0
- Initial version